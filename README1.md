OUR SURVEY
We conducted a large survey from January 13 to 16, 2019, among 787 global travelers, in which we asked them which backpack they chose, what they think of it, where they bought it... We will refer to it regularly in this file. Learn more about our methodology

ONE OR TWO BAGS?
Almost all global travelers leave with two backpacks, a big one (from 40 to 70 liters) and a small one (from 10 to 20 liters). This allows you to leave the big bag in your room during the day and take only the small bag to go to the beach, to visit places, or for short hikes. You only move with the big bag when you change location.
https://soismoi.com
On long bus or plane trips, you can leave the large bag in the hold and keep the small bag containing valuables (camera, smartphone...) with you.

During these long trips, avoid carrying the small bag in front of you. Plan the necessary space to put it inside your big bag. Indeed, a good hiking bag has lumbar supports and a belly strap. It's much more comfortable and will force you to limit the total weight of your load.

WHICH DAYPACK TO USE?
We often underestimate the weight and volume reduction that can be achieved by choosing the right booster bag. For example, the best-selling small Quechua bag, the 20-liter NH100, weighs 340 grams and takes up a lot of space inside your large bag. It's way too heavy and too big.

TRAVEL BAG WITH A REMOVABLE BUILT-IN SIDE BAG
Some big bags have a small removable bag that zips onto the big bag (Deuter Avant Voyager, Osprey Farpoint) or a top pocket that can be transformed into a spare bag (Osprey Aether AG and Forclaz Travel 500).

This is very practical, it avoids having to find space in the big bag for every long trip. You can quickly detach it to get on a bus while leaving the big bag in the hold.

Zippable backpacks have a big disadvantage, it is that they move the center of gravity of the bag away from your back and therefore they pull more on your shoulders. They also make it a little harder to get inside the bag.

Integrated removable duffel bags are generally quite small, closer to 10 liters than 20 liters, and their weight is around 200 grams, which is not that heavy, but improvable.

Many travelers like this solution, but François prefers to take a separate bag and therefore buy a lighter travel bag without this option.

OUR SOLUTION: ULTRA-LIGHT AND COMPRESSIBLE DUFFEL BAGS.
They are resistant, waterproof (you could almost swim with them), and best of all, they are compressible to the point that you can fit them in a small integrated pocket, the size of your hand. The final plus is that you can make a small travel pillow by compressing the air inside.

The only real drawback is that they don't have a front zipper. So you have to fumble around at the top to find what you're looking for in the bottom of the bag, which isn't very practical.

These ultra-compact bags represent a weight saving of up to 1 kilo compared to a small classic backpack, not to mention the enormous space-saving.